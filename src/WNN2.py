# -*- coding: utf-8 -*-
"""
Created on Fri May  8 20:07:52 2020

@author: King
"""

#package importálás
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
#adatsor beolvasása
###############################
#X (bemenet) és Y(kimenet) értékek, adatsortól függ hogyan
data = loadmat("adapt.mat")   #ha .mat fájlból töltjük fel, ez a gyakorlat anyaga
X = data["X"] # a fájlból kiszedjük a bemeneteket

y = data["Y"]*10 # és a cimkéket
del data # majd töröljük
###############################
#aktivációs függvények
###############################
def sigmoid(z):
    g = 1/(1+np.exp(-z))    
    return g
def sigmoidGradient(z):  
    g_deriv=sigmoid(z)*(np.ones(np.shape(z))-sigmoid(z))
    return g_deriv
def gauss(z):
    g = -z*np.exp(-0.5*np.square(z))
    return g
def gaussGradient(z):
    g_deriv=(z-np.ones(np.shape(z)))*np.exp(-0.5*np.square(z))
    return g_deriv
def MexicanHat(z):
    g = (np.ones(np.shape(z))-np.square(z))*np.exp(-0.5*np.square(z))
    return g
def MexicanHatGradient(z):
    g_deriv = -np.exp(-0.5*np.square(z))-2*z*np.exp(-0.5*np.square(z))+np.square(z)*np.exp(-0.5*np.square(z))
    return g_deriv
def MorletWavelet(z):
    g = np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g
def MorletWaveletGradient(z):
    g_deriv = -5*np.sin(5*z)*np.exp(-0.5*np.square(z))-np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g_deriv    
###############################
# szükséges mátrixok: 
#X bemeneti    n*(i)
#Y kimeneti    n*h 
#wk1_ij  - bemenet-rejtett neuronok transzlációs mátrixa (i*j) (j db rejtett neuron)
#we1_ij  - bemenet-rejtett neuronok dilatációs mátrixa   (i*j) (j db rejtett neuron)
#w2   -  rejtett neuronok-kimenet súlyai (j)*h 
#w2_b -  a rejtett réteg Bias tagja 1*h 
#w0   -  bemenet-kimenet közvetlen súlyai (i)*h
#Phi    - wavelet function mátrixa wk1_ij, we1_ij és X felhasználásával (i+1)*j
#X -> bolvasott bemenetek mátrixa
#y -> beolvasott kimenetek oszlopvaktora 
                             # visszaadjuk a normalizált X-et, átlagot, szórást

j = 10 #10db rejtett neuron
WK1_ij = np.ones((np.shape(X)[1], j))
WE1_ij = np.ones((np.shape(X)[1], j))
W2 = np.ones((j, np.shape(y)[1]))
W2_d = np.ones((1, np.shape(y)[1])) 
W0 = np.ones((np.shape(X)[1], np.shape(y)[1]))
def featureNormalization(X):
#######################################    
    mean = np.mean(X,axis=0)                               # X oszlopainak átlagai
    std = np.std(X, axis=0,ddof=1)                         # X oszlopainak szórásai (korrigált tapasztalati)                                        

    X_norm = (X - mean) / std                              # a nem volt, akkor csak elvégezzük a normalizálást
#######################################
    return np.array(X_norm) 
X_norm = featureNormalization(X)
#mátrixok
####################
for i in range(X.shape[1]): # cikk alapján a súlyok kezdeti inicializálása    
    WK1_ij[i,:] = 0.5*(np.amax(X[:, i])+np.amin(X[:, i]))
    WE1_ij[i,:] = 0.2*(np.amax(X[:, i])-np.amin(X[:, i]))
# w2 és w0 0 és 1 között tetszőleges érték    
def randInitializeWeights(L_in, L_out):
    epsilon_init = 1
    W = np.random.rand(L_out,L_in)*(2*epsilon_init)
    return W.T
W2 = randInitializeWeights(j, np.shape(y)[1])
def Phi(WK1_ij, WE1_ij, X): # HN-hidden neurons = np.shape(wk1_ij[1]) csak a követhetőségért
                            # a cikk alapján minden eleme egy 10 elemből álló szorzat
                            #amely szorzat elemeinek a deiváltjai is kellenek majd külön-külön, ezért van így megoldva
                            #i+1 iteráció, minden iterációnal meghatározzuk az adott elemekhez a bemenetet az aktivációs függvényhez
                            #így Z mátrix dimenzói: n*j*(i+1), és a tényleges kimenet a rejtett rétegben
                            #a Z mátrix i+1-es dimenziójában a megfelelő elemek összeszorzása
                            #vagyis a Phi rejtett réteg kimenete egy n*j mdimenziós mátrix
                            # FI kimenet már az aktivált szorzat, Z a nem aktivált, FId pedig a derivált aktivációs függvénnyel aktivált szorzat
    m = np.shape(X)[0]
    n = np.shape(X)[1] # = np.shape(WK1_ij[0]) 
    HN = np.shape(WK1_ij)[1]
    Z = []
    FI = np.ones((m, HN))
    FId = np.ones((m, HN))
    for i in range(n):
        seged = np.zeros((m, HN))
        for k in range(m):
            for l in range(HN):
                seged[k, l] = (X[k, i]-WK1_ij[i, l])/WE1_ij[i, l]
           
        Z.append(seged)
        FI = FI*gauss(seged)
    FId=np.array(gaussGradient(Z))
    Z = np.array(Z)
    
    return Z, FI, FId

####################
#cost, grad function
def CostGrad(X,y,w0, wk1_ij,we1_ij,w2, w2_d,class_num, Lambda):
    m = np.shape(X)[0]
    n = np.shape(wk1_ij)[0]
    Z, FI, FID =Phi(wk1_ij, we1_ij, X)
    HN_bias = np.ones((np.shape(y)[0], 1))
   #Forward step
   #####################   
    
    Y1 = FI.dot(w2)
    Y2 = X.dot(w0)
    Y3 = HN_bias*w2_d
    
    Ypred = Y1+Y2+Y3
  
    ####################
    #ONE HOT ENCODING
    ####################
#    Y=np.zeros((len(y),class_num))
#    #pred_formed=np.zeros((len(pred),num_labels))
#    for i in range(y.shape[0]):
#        value=y[i, 0]-1 # ha 1-től kezdve nevezzük el a kimeneteket
#        Y[i,value] = value
    ####################
    #Penalties for the weights (for COST function)
    #################### #BIAS tagokkal nem büntetjük
    pen_wk1_ij = (np.sum((wk1_ij)**2))*Lambda/2/m 
    pen_we1_ij = (np.sum((we1_ij)**2))*Lambda/2/m 
    pen_w2 = (np.sum(w2**2))*Lambda/2/m 
    pen_w0 = (np.sum((w0)**2))*Lambda/2/m 
    ####################
    #Punishment for learning
    ####################

    pun_wk1 = (Lambda/m)*wk1_ij 
    pun_we1 = (Lambda/m)*we1_ij
    pun_w2 = (Lambda/m)*w2
    pun_w0 = (Lambda/m)*w0
    
    ####################
    #COST function ????????
    ####################
    diff = y - Ypred
    
    C=np.sum(diff**2)/(m*2)+pen_wk1_ij+pen_we1_ij+pen_w2+pen_w0
    ####################
    #Gradients
    ####################
    grad_w0 = -(diff.T.dot(X)).T/m + pun_w0
    grad_w2 = (-1)/m*(diff.T.dot(FI)).T + pun_w2
    grad_w2_d = -diff*(HN_bias)/m
    grad_wk1 = np.zeros(np.shape(wk1_ij)) 
    grad_we1 = np.zeros(np.shape(we1_ij))      
   
    for i in range(n):
            grad_wk1[i, :] = np.sum(-diff.dot(w2.T)*((FI/gauss(Z[i])*FID[i])), axis=0)/m/we1_ij[i, :]
            grad_we1[i, :] = np.sum(-diff.dot(w2.T)*((FI/gauss(Z[i])*FID[i]*Z[i])), axis=0)/m/we1_ij[i, :]
    grad_wk1 = grad_wk1 + pun_wk1
    grad_we1 = grad_we1 + pun_we1        
    


    
    
    ####################
    return C,grad_w0, grad_wk1, grad_we1, grad_w2, grad_w2_d, Ypred
#Tanítás
####################
def gradientDescentnn(X, y, wk1, we1, w2, w0, w2_d, lr_rate, num_labels, Lambda, num_iters):

    WK1 = wk1
    WE1 = we1
    W2 = w2
    W2_d = w2_d
    W0 = w0
  
    C_history = []

    for i in range(num_iters):
        print('Iteration:', i+1)
        
           
        
        
        C,grad_w0, grad_wk, grad_we, grad_w2, grad_w2_d, Ypred= CostGrad(X,y,W0, WK1,WE1,W2,W2_d,num_labels, Lambda)
        
        WK1 = WK1 - (lr_rate * grad_wk)
        WE1 = WE1 - (lr_rate * grad_we)
        W2 = W2 - (lr_rate * grad_w2)
        W2_d = W2_d - (lr_rate * grad_w2_d)
        W0 = W0 - (lr_rate * grad_w0)
        C_history.append(C)
   
    return C_history,W0, WK1, WE1, W2, W2_d, Ypred
CC,w0_pred, wk1_pred, we1_pred, w2_pred, w2_d_pred, Ypred = gradientDescentnn(X_norm, y, WK1_ij, WE1_ij, W2, W0, W2_d, 0.05, 1, 0, 100)
plt.plot(CC)
plt.xlabel('Iteration')
plt.ylabel('Cost function')
plt.title('Cost function over the iterations (Current params)')
plt.show()
plt.plot(np.abs(Ypred))
#def predict(wk1,we1,w2,X):
#    m = X.shape[0]
#    #Forward step
#    ########################################################
#    Z, FI, FID =Phi(wk1, we1, X)  
#    FI_bias = np.column_stack((np.ones(m), FI))
#    Y1 = FI_bias.dot(w2)
#    
#    print(Y1)
#    Ypred = sigmoid(Y1)
#    p = np.argmax(Ypred, axis=1) 
#    
#    ########################################################
#    return p
#def accuracy(pred,y):
#    acc = (np.mean(pred[:,np.newaxis]==y))*100
#    return acc
#pred = predict(wk1_pred, we1_pred ,w2_pred,X)
#acc = accuracy(pred,y)
#
#print('Training Set Accuracy after :%.0f' % (acc))