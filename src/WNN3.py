# -*- coding: utf-8 -*-
"""
Created on Fri May  8 20:07:52 2020

@author: King
"""

#package importálás
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
#adatsor beolvasása
###############################
#X (bemenet) és Y(kimenet) értékek, adatsortól függ hogyan
data = loadmat("adapt.mat")   #ha .mat fájlból töltjük fel, ez a gyakorlat anyaga
X = data["X"] # a fájlból kiszedjük a bemeneteket

y = data["Y"] # és a cimkéket
del data # majd töröljük
###############################
#aktivációs függvények
###############################
def sigmoid(z):
    g = 1/(1+np.exp(-z))    
    return g
def sigmoidGradient(z):  
    g_deriv=sigmoid(z)*(np.ones(np.shape(z))-sigmoid(z))
    return g_deriv
def gauss(z):
    g = -z*np.exp(-0.5*np.square(z))
    return g
def gaussGradient(z):
    g_deriv=(z-np.ones(np.shape(z)))*np.exp(-0.5*np.square(z))
    return g_deriv
def MexicanHat(z):
    g = (np.ones(np.shape(z))-np.square(z))*np.exp(-0.5*np.square(z))
    return g
def MexicanHatGradient(z):
    g_deriv = -np.exp(-0.5*np.square(z))-2*z*np.exp(-0.5*np.square(z))+np.square(z)*np.exp(-0.5*np.square(z))
    return g_deriv
def MorletWavelet(z):
    g = np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g
def MorletWaveletGradient(z):
    g_deriv = -5*np.sin(5*z)*np.exp(-0.5*np.square(z))-np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g_deriv    
###############################
# szükséges mátrixok: 
#X bemeneti    n*(i)
#Y kimeneti    n*h 
#wk1_ij  - bemenet-rejtett neuronok transzlációs mátrixa (i*j) (j db rejtett neuron)
#we1_ij  - bemenet-rejtett neuronok dilatációs mátrixa   (i*j) (j db rejtett neuron)
#w2   -  rejtett neuronok-kimenet súlyai (j)*h 
#w2_b -  a rejtett réteg Bias tagja 1*h 
#w0   -  bemenet-kimenet közvetlen súlyai (i)*h
#Phi    - wavelet function mátrixa wk1_ij, we1_ij és X felhasználásával (i+1)*j
#X -> bolvasott bemenetek mátrixa
#y -> beolvasott kimenetek oszlopvaktora 
                             # visszaadjuk a normalizált X-et, átlagot, szórást

j = 10 #10db rejtett neuron
class_num = 4
WK1_ij = np.ones((np.shape(X)[1], j))
WE1_ij = np.ones((np.shape(X)[1], j))
W2 = np.ones((j, np.shape(y)[1]))
W2_d = np.ones((1, class_num)) 
W0 = np.ones((np.shape(X)[1], class_num))
def featureNormalization(X):
#######################################    
    mean = np.mean(X,axis=0)                               # X oszlopainak átlagai
    std = np.std(X, axis=0,ddof=1)                         # X oszlopainak szórásai (korrigált tapasztalati)                                        

    X_norm = (X - mean) / std                              # a nem volt, akkor csak elvégezzük a normalizálást
#######################################
    return np.array(X_norm) 
X_norm = featureNormalization(X)
#mátrixok
####################
for i in range(X.shape[1]): # cikk alapján a súlyok kezdeti inicializálása    
    WK1_ij[i,:] = 0.5*(np.amax(X_norm[:, i])+np.amin(X_norm[:, i]))
    WE1_ij[i,:] = 0.2*(np.amax(X_norm[:, i])-np.amin(X_norm[:, i]))
# w2 és w0 0 és 1 között tetszőleges érték    
def randInitializeWeights(L_in, L_out):
    epsilon_init = 1
    W = np.random.rand(L_out,L_in)*(2*epsilon_init)
    return W.T
W2 = randInitializeWeights(j, class_num)
def Phi(WK1_ij, WE1_ij, X): # HN-hidden neurons = np.shape(wk1_ij[1]) csak a követhetőségért
                            # a cikk alapján minden eleme egy 10 elemből álló szorzat
                            #amely szorzat elemeinek a deiváltjai is kellenek majd külön-külön, ezért van így megoldva
                            #i+1 iteráció, minden iterációnal meghatározzuk az adott elemekhez a bemenetet az aktivációs függvényhez
                            #így Z mátrix dimenzói: n*j*(i+1), és a tényleges kimenet a rejtett rétegben
                            #a Z mátrix i+1-es dimenziójában a megfelelő elemek összeszorzása
                            #vagyis a Phi rejtett réteg kimenete egy n*j mdimenziós mátrix
                            # FI kimenet már az aktivált szorzat, Z a nem aktivált, FId pedig a derivált aktivációs függvénnyel aktivált szorzat
    m = np.shape(X)[0]
    n = np.shape(X)[1] # = np.shape(WK1_ij[0]) 
    HN = np.shape(WK1_ij)[1]
    Z = []
    FI = np.ones((m, HN))
    FId = np.ones((m, HN))
    for i in range(n):
        seged = np.zeros((m, HN))
        for k in range(m):
            for l in range(HN):
                seged[k, l] = (X[k, i]-WK1_ij[i, l])/WE1_ij[i, l]
           
        Z.append(seged)
        FI = FI*gauss(seged)
    FId=np.array(gaussGradient(Z))
    Z = np.array(Z)
    
    return Z, FI, FId

####################
#cost, grad function
def CostGrad(X,y,w0, wk1_ij,we1_ij,w2, w2_d,class_num, Lambda):
    m = np.shape(X)[0]
    n = np.shape(wk1_ij)[0]
    Z, FI, FID =Phi(wk1_ij, we1_ij, X)
    HN_bias = np.ones((np.shape(y)[0], 1))
   #Forward step
   #####################   
    
    Y1 = FI.dot(w2)
    Y2 = X.dot(w0)
    Y3 = HN_bias*w2_d
    
    Ypred = Y1+Y2+Y3
    Ypred2 = sigmoid(Ypred)
    ####################
    #ONE HOT ENCODING
    ####################
    Y=np.zeros((len(y),class_num))
    for i in range(y.shape[0]):
        value=y[i, 0] # ha 1-től kezdve nevezzük el a kimeneteket
        Y[i,value] = 1
    ####################
    #Penalties for the weights (for COST function)
    #################### #BIAS tagokkal nem büntetjük
    pen_wk1_ij = (np.sum((wk1_ij)**2))*Lambda/2/m 
    pen_we1_ij = (np.sum((we1_ij)**2))*Lambda/2/m 
    pen_w2 = (np.sum(w2**2))*Lambda/2/m 
    pen_w0 = (np.sum((w0)**2))*Lambda/2/m 
    ####################
    #Punishment for learning
    ####################

    pun_wk1 = (Lambda/m)*wk1_ij 
    pun_we1 = (Lambda/m)*we1_ij
    pun_w2 = (Lambda/m)*w2
    pun_w0 = (Lambda/m)*w0
    
    ####################
    #COST function 
    ####################
   
    first_member=Y*np.log(Ypred2) #kiválogatjuk  a megfelelő értékeket az aktivált kimenetekből
    second_member=np.subtract(np.ones(np.shape(Y)),Y)*np.subtract(np.ones(np.shape(Y)),np.log(Ypred2))
    C=np.sum(first_member*second_member,axis=None)/2/(-m)+pen_wk1_ij+pen_we1_ij+pen_w2+pen_w0
    ####################
    #Gradients
    ####################
    diff = Y-Ypred2
    SG = sigmoidGradient(Ypred)
    diff2 = diff*SG
    grad_w0 = -(X.T.dot(diff2))/m + pun_w0
    grad_w2 = (-1)/m*(FI.T.dot(diff2)) + pun_w2
    grad_w2_d = -diff2*(HN_bias)/m
    grad_wk1 = np.zeros(np.shape(wk1_ij)) 
    grad_we1 = np.zeros(np.shape(we1_ij))      
   
    for i in range(n):
        FI_dd = np.ones(np.shape(FI))
        for p in range(n):
            if p == i:
                FI_dd =FI_dd*gaussGradient(Z[p])
            else:
                FI_dd =FI_dd*gauss(Z[p])
        grad_wk1[i, :] = np.sum(-diff2.dot(w2.T)*(FI_dd), axis=0)/m/we1_ij[i, :]
        grad_we1[i, :] = np.sum(-diff2.dot(w2.T)*((FI_dd*Z[i])), axis=0)/m/we1_ij[i, :]
    grad_wk1 = grad_wk1 + pun_wk1
    grad_we1 = grad_we1 + pun_we1        
   


    
    
    ####################
    return C,grad_w0, grad_wk1, grad_we1, grad_w2, grad_w2_d, Ypred2
#Tanítás
####################
def gradientDescentnn(X, y, wk1, we1, w2, w0, w2_d, lr_rate, num_labels, Lambda, num_iters):

    WK1 = wk1
    WE1 = we1
    W2 = w2
    W2_d = w2_d
    W0 = w0
  
    C_history = []
    
    for i in range(num_iters):
        
        print('Iteration:', i+1)
        
        
        
        C,grad_w0, grad_wk, grad_we, grad_w2, grad_w2_d, Ypred2= CostGrad(X,y,W0, WK1,WE1,W2,W2_d,num_labels, Lambda)
        
        WK1 = WK1 - (lr_rate * grad_wk)
        WE1 = WE1 - (lr_rate * grad_we)
        W2 = W2 - (lr_rate * grad_w2)
        W2_d = W2_d - (lr_rate * grad_w2_d)
        W0 = W0 - (lr_rate * grad_w0)
        C_history.append(C)
    Ypred = np.argmax(Ypred2, axis = 1)    
    return C_history,W0, WK1, WE1, W2, W2_d, Ypred
num_iters = 100
CC,w0_pred, wk1_pred, we1_pred, w2_pred, w2_d_pred, Ypred = gradientDescentnn(X_norm, y, WK1_ij, WE1_ij, W2, W0, W2_d, 300, 4, 1, num_iters)
plt.plot(CC)
plt.xlabel('Iteration')
plt.ylabel('Cost function')
plt.title('Cost function over the iterations (Current params)')
plt.show()
plt.plot(Ypred, 'bo')
plt.show()
# Pontosság értékelése
def accuracy(pred,y):
    acc = (np.mean(pred[:,np.newaxis]==y))*100
    return acc
acc = accuracy(Ypred,y)

print('Training Set Accuracy after 100 iteration :%.0f' % acc)