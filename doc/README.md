# Useful scientific articles
Classification of EEG Signals using adaptive weighted distance nearest neighbor algorithm - E. Parvinnia a, *, M. Sabeti a , M. Zolghadri Jahromi b , R. Boostani b 
  
Drowsy Driving Detection by EEG Analysis Using Wavelet Transform and K-Means Clustering - Nikita Gurudath, H. Bryan Riley  
  
Epileptic seizure identification from electroencephalography signal using DE-RBFNs ensemble - Satchidanada Dehuria *, Alok Kumar Jagadevb , and Sung-Bae Choc  
  
Wavelet Neural Networs: A Practical Guide - Antonios K. Alexandridis1, Achilleas D. Zapranis2  
  
Learners’ Learning Style Classification related to IQ and Stress based on EEG - Nazre Abdul Rashid, Mohd. Nasir Taib, Sahrim Lias, Norizam Sulaiman,
Zunairah Hj. Murat, Ros Shilawani S. Abdul Kadir  
  
Sugihara causality analysis of scalp EEG for detection of early Alzheimer's disease - Joseph C. McBridea, Xiaopeng Zhao, Nancy B. Munroc, Gregory A. Jichad, Frederick A. Schmittd,
Richard J. Krysciod, Charles D. Smithd, Yang Jiangd  
  
Analysis and identification of the EEG signals from visual stimulation - Mineyuki Tsuda, Yankun Lang, Haiyuan Wu  
  
On the use of wavelet neural networks in the task of epileptic seizure detection from electroencephalography signals - Zarita Zainuddin, Lai Kee Huong, Ong Pauline  
  
