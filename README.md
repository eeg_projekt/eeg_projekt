# EEG_projekt
EEG jelek klasszifikációja neurális hálózattal  
# src -> a projekt kódjait, adatfájljait tartalmazza  
    WNN2 -> kimenet a cikk alapján, nincs ONE HOT CODING  
    WNN3 -> kimenetet szigmoid függvénnyel aktiváltuk a több osztályos klasszifikációhoz  
    adapt.mat -> mintaadatsor tartalma 2000x64-es bemeneti X mátrix és 2000x1-es kimeneti Y mátrix  
    WNN3 92%-os hatékonysággal klasszifikál 250 ciklus után  
    WNN2-nél nincs megoldva a klasszifikáció  
# doc -> a projekthez tartozó irodalom, illetve a projekt összefoglalója és dokumentációja  
    WNN.pdf dokumentum alapján készült a program  
    Projekt_dokumentacio.pdf -> a projekt dokumentációja  
    Projekt_osszefoglalo.pdf -> a projekt rövid összefoglalója a projekten dolgozók illetve a tárgy megnevezésével a nehézségek, hibák, fejlesztési lehetőségek